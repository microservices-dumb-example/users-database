/*
   baseline.sql database baseline
   Copyright (C) 2020 Bruno Mondelo

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

CREATE TABLE {{schema}}.refresh_tokens (
    owner BIGINT NOT NULL,
    token TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE
        DEFAULT (NOW() AT TIME ZONE 'utc') NOT NULL,
    expires_at TIMESTAMP WITH TIME ZONE NOT NULL,

    PRIMARY KEY(owner, token),
    FOREIGN KEY(owner) REFERENCES {{schema}}.users(id) ON DELETE RESTRICT
        ON UPDATE RESTRICT
);

INSERT INTO {{schema}}.schema_version VALUES(
    DEFAULT,
    1,
    1,
    0,
    'Created table containing refresh tokens',
    DEFAULT
);
